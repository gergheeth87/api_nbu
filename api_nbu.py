import requests
from datetime import datetime

nbu_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

headers = {
    'accept': 'application/json',
    'content-type': 'application/json'
}


def time_and_date():
    time_date = datetime.now()
    t_d_now = (time_date.strftime('<%d %b %Y (%H:%M:%S)>'))
    return t_d_now


try:
    response = requests.request('GET', nbu_url, headers=headers)
except ConnectionError as exc:
    raise RuntimeError('Failed to open database') from exc
else:
    print(response.status_code)
    if 300 > response.status_code >= 200:
        all_data = (response.json())
        count = 0
        with open('nbu_values_now', 'w') as file:
            file.write(f'{time_and_date()} {'\n'}')
        for i in all_data:
            count += 1
            data_dict = {}
            data_dict.update(i)
            with open('nbu_values_now', 'a') as file:
                file.write(f'{count}. {data_dict['cc']} - {data_dict['rate']} {'\n'}')
