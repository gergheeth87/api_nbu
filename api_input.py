import requests


def receiving_user_data():
    while True:
        time_and_date = input('Enter yyyymmdd: ')
        if time_and_date.isdigit():
            if len(time_and_date) != 8:
                print('Must be 8 digits, try again')
            else:
                break
        else:
            print('Enter only digits in the format yyyymmdd, try again')
    return time_and_date


def data_for_writing(time_and_date):
    data_writing = list(time_and_date)
    data_writing.insert(4, ' ')
    data_writing.insert(7, ' ')
    data_writing.insert(0, '<')
    data_writing.insert(11, '>')
    str_data_writing = str(data_writing)
    new_str = str_data_writing.replace("'", "")
    new_str2 = new_str.replace(",", "")
    new_str3 = new_str2.replace("[", "")
    new_str4 = new_str3.replace("]", "")
    return new_str4


nbu_url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={receiving_user_data()}&json'

headers = {
    'accept': 'application/json',
    'content-type': 'application/json',
}

try:
    response = requests.request('GET', nbu_url, headers=headers)
except ConnectionError as exc:
    raise RuntimeError('Failed to open database') from exc
else:
    if 300 > response.status_code >= 200:
        input_data = (response.json())
        count = 0
        with open('nbu_values_data', 'w') as file:
            file.write(f'<{data_for_writing(receiving_user_data())}>{'\n'}')
        for i in input_data:
            count += 1
            input_data_dict = {}
            input_data_dict.update(i)
            with open('nbu_values_data', 'a') as file:
                file.write(f'{count}. {input_data_dict['cc']} - {input_data_dict['rate']} {'\n'}')
